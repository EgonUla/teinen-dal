﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassStruct
{
    class Program
    {
        static void Main(string[] args)
        {
            // kui inimene on kalss siis 
            InimeneClass esimeneC = new InimeneClass() ;
            esimeneC.Nimi = "Henn";
            esimeneC.Vanus = 64;
            InimeneClass teineC = esimeneC;
            teineC.Nimi = "Sarviktaat";
            Console.WriteLine(esimeneC.Nimi);
            Console.WriteLine(teineC.Nimi);


            // kui inimene on struct siis igas muutujas on oma eksemplar
            InimeneStruct esimeneS;
            esimeneS.Nimi = "Henn";
            esimeneS.Vanus = 64;
            InimeneStruct teineS = esimeneS;
            teineS.Nimi = "Sarviktaat";
            Console.WriteLine(esimeneS.Nimi);
            Console.WriteLine(teineS.Nimi);


            //int[] arvud = { 1, 2, 3, 4, 5 };
            //int[] teised = (int[])arvud.Clone();
            //teised[2] = 7;
            //Console.WriteLine($"[{string.Join(",", arvud)}]");



        }
    }


    struct InimeneStruct
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene (struct) {Nimi} vanusega {Vanus}";

    }

    class InimeneClass
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene (class) {Nimi} vanusega {Vanus}";
    }



}
