﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidMeetodid
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene mina = new Inimene();

            mina.Nimi = "Henn";
            mina.Vanus = 64;
            Console.WriteLine(mina);

            Inimene teineMina = new Inimene() 
            { 
                Nimi = "Henn", 
                Vanus = 64 
            };

            Console.WriteLine(teineMina);

            Inimene laps = new Inimene
            {
                Nimi ="Kalle",
                Vanus= 13
            };

            Inimene.MyyViinaStatic(laps);
            mina.MyyViinaInstance();

        }
    }






    class Inimene
    {
        public string Nimi;
        public int Vanus;
        public static int VanusePiir = 18;

        public override string ToString()
        {
            return $"Inimene {Nimi} vanusega {Vanus}";
        }

        public static void MyyViinaStatic(Inimene x)
        {

            if (x.KasLaps()) Console.WriteLine($"{x.Nimi} viina ei saa");
            else Console.WriteLine($"No osta siis viina {x.Nimi}");
        }

        public void MyyViinaInstance(Inimene x)
        {

            if (this.KasLaps()) Console.WriteLine($"{this.Nimi} viina ei saa");
            else Console.WriteLine($"No osta siis viina {this.Nimi}");
        }


        public bool KasLaps()
        {

            return Vanus < VanusePiir;
        }



    }



}
