﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassidaccess
{

   
    class Program
    {
        static void Main(string[] args)
        {


            Inimene henn = new Inimene ("39011111111", "Henn")
            {

               Nimi = "Henn",
                // IK = "11111111"
            };


            new Inimene("1111111111", "Mai");

            
            Console.WriteLine(henn.Nimi);
            Console.WriteLine(henn.Dateofbirth);
            Console.WriteLine(henn.Gender);
        }
    }


    enum Gender { Female, Male };
    class Inimene
    {

        // static väljad

        static int _InimesteArv = 0;



        public static int InimesteArv => _InimesteArv;

        private int _InimeseNr = ++_InimesteArv;


        private string _IK;
        public string _Nimi;


        // konstruktorid
        public  Inimene(string IK) => this._IK = IK;

         public Inimene(string IK, string nimi) => (this._IK, this.Nimi) = (IK, nimi); // constructor overload
            
      
        public override string ToString() =>  $"Inimene {_Nimi} (IK={_IK})";

        public string GetIK() => _IK;
        public string GetNimi() => _Nimi;

        public void SetNimi(string uusnimi) => _Nimi = ToProper(uusnimi);

        public string Nimi
        {
            get => _Nimi;

            set => _Nimi = ToProper(value);
            
        }


        public DateTime Dateofbirth
        {

            get
            {
                int sajand = ((_IK[0] - '1')/2) + 18;

                return new DateTime(
                    sajand * 100 +
                    int.Parse(IK.Substring(1, 2)),
                    int.Parse(IK.Substring(3, 2)),
                    int.Parse(IK.Substring(5, 2))

                    ) ;
            }
        }



        public Gender Gender => (Gender)(_IK[0] % 2);



        public string IK => _IK;
            
        
        public static string ToProper(string tekst) => tekst == "" ? "" : tekst.Substring(0,1).ToUpper() + tekst.Substring(1).ToLower();
    }
}
