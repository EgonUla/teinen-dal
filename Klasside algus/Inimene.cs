﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klasside_algus
{
    class Inimene
    {

        public string Nimi;
        public int? Vanus;
        public int KingaNumber; // TODO: hiljem korjame siit ülearused ära

        public override string ToString()
        {
            return $"Inimene {Nimi} vanusega {(Vanus?.ToString() ?? "mida me ei tea")}";
        }

        public void Trüki()
        {
         Console.WriteLine(this);
        }

        public int? Oletatavsünniaasta()
        {

            return DateTime.Now.Year - Vanus;
        }
    }
}
