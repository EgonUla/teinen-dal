﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// TODO: hiljem korjame siit ülearused ära


namespace Klasside_algus
{
    class Program
    {
        static void Main(string[] args)
        {

            //Ütletere();
            //Ütletere("Henn");
            //Tervitapikalt("Henn", "Sarv");


            Inimene mina = new Inimene();
            mina.Nimi = "Henn";
            mina.Vanus = 64;

            Inimene tema = new Inimene();
            tema.Nimi = "Raja Teele";
            Console.WriteLine(tema.Vanus);

            Inimene[] inimesed = new Inimene[10];
            inimesed[0] = mina;
            inimesed[1] = tema;


            // Console.WriteLine(mina);
            // Console.WriteLine(tema);

            //mina.Trüki();

            //Console.WriteLine(mina.Oletatavsünniaasta());




            string teks = Console.ReadLine();
            Console.WriteLine(ToProper(teks));

            

        }

        static void Ütletere()
        {

            Console.WriteLine("Tere!");
        }

        static void Ütletere(string nimi)
        {

            Console.WriteLine($"Tere {nimi}!");
        }

        static void Tervitapikalt(string eesNimi, string pereNimi)
        {

            Console.WriteLine($"Tere {eesNimi}!");
            Console.WriteLine($"Mul on hea meel näha kedagi perekonnast {pereNimi}!");
        }


        static string ToProperx(string tekst)
        {


           return tekst == "" ? "" : tekst.Substring(0,1).ToString().ToUpper() + tekst.ToLower().Substring(1);

        }


        static string ToProper(string tekst)
        {

            string[] osad = tekst.Split(' ');

            for (int i = 0; i < osad.Length; i++)
            {
                osad[i] = ToProperx(osad[i]);
            }


            return string.Join(" ", osad);

        }




    }

}

    