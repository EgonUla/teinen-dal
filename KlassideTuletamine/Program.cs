﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {

            Loom kroko = new Loom { Liik = "krokodill" };
            //kroko.TeeHäält();
            Loom ämmelgas = new Loom("ämblik");
            Loom elajas = new Loom();
            //elajas.TeeHäält();

            
            Kass k = new Kass("Miisu", "Angoora");
            // k.TeeHäält();

            List<Loom> loomad = new List<Loom>
            {

                kroko, ämmelgas, elajas, k
            };


            

            foreach (var x in loomad) x.TeeHäält();
            foreach (var x in loomad) Console.WriteLine(x); 
            foreach (var x in loomad) x.SikutaSabast(); 



            k.Silita();
            k.TeeHäält();
            k.SikutaSabast();
            k.TeeHäält();



        }
    }



    class Loom
    {
        public string Liik;

        public Loom(string liik) => Liik = liik;

        public Loom() : this("tundmatu") { }

        public virtual void TeeHäält() => Console.WriteLine($"{Liik} teeb koledat häält");

        public void SikutaSabast() => Console.WriteLine($"HOIATUS! kui sikutada {Liik} sabast, siis see ei ole hea");
        
    }


    class Koduloom : Loom
    {
        public string Nimi;

        public Koduloom(string liik, string nimi) : base(liik) => Nimi = nimi;

        public override void TeeHäält() => Console.WriteLine($"{Nimi} mörin" );

        public override string ToString()
        {
            return "kodu" + base.ToString();
        }
    }

    class Kass : Koduloom
    {

        public string Tõug;
        public bool Tuju = false;

        public Kass(string Nimi, string tõug) : base("Kass", Nimi) => Tõug = tõug;

        public void Silita() => Tuju = true;

        public void SikutaSabast() => Tuju = false;


        public override void TeeHäält()
        {
            if (Tuju) Console.WriteLine($"Kass {Nimi} lööb nurru");
            else Console.WriteLine($"{Nimi} Kräu");
        }

    }

   interface ISöödav
    {

        void süüakse();
    }



}
