﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tuletamise_ülesanne
{
    class Program
    {
        static void Main(string[] args)
        {

            //Õpilane juku = new Õpilane("1A", "Juku");
            //Õpilane maku = new Õpilane("2A", "Maku");

            
            //Õpetaja malle = new Õpetaja("matemaatika", "Malle");
            //Õpetaja Henn = new Õpetaja("Keemia", "Henn");

           


            List<Inimene> nimekiri = new List<Inimene>
            {

                new Õpilane("1A", "Juku"),
                new Õpilane("2A", "Maku"),
                new Õpetaja("matemaatika", "Malle"),
                new Õpetaja("keemia", "Henn")
            };



            foreach (var x in nimekiri) x.TeeHäält();
            

        }
    }

    class Inimene
    {
        public string Nimi;
        public Inimene(string nimi) => Nimi = nimi;
        public virtual void TeeHäält() => Console.WriteLine(Nimi);
    }


    class Õpilane : Inimene
    {

        public string Klass;

        public Õpilane(string klass, string nimi) : base(nimi) => Klass = klass;
        public override void TeeHäält() => Console.WriteLine($"{Klass} klassi õpilane {Nimi}");

    }

    class Õpetaja : Inimene
    {
        public string Aine;
        public Õpetaja(string aine, string nimi) : base(nimi) => Aine = aine;
        public override void TeeHäält() => Console.WriteLine($"{Aine} õpetaja {Nimi}");

    }




}
